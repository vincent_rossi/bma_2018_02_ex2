## Bataille Navale - Vérifier une grille
La bataille navale est un jeu dans lequel vous devez placer vos différents navires sur une grille 10x10. Ces navires doivent être placés soit **verticalement**, soit **horizontalement**.
Chaque navire est identifiable par une lettre et une taille.

Voici quelques questions à se poser pour valider qu'une grille respecte bien les règles du jeu :
* Tous les bateaux sont-ils présents 1 fois uniquement?
* Ont-ils bien la bonne taille ?
* etc...

Dans cette exercice, vous devrez vérifier que la grille que nous vous fournirons est bien valide.
La grille est une matrice à 2 dimensions contenant des lettres.
Cette matrice est toujours de taille 10x10 (inutile donc de vérifier les dimensions).

La mer est représentée par le caractère '~' alors que les bateaux ont tous une lettre associée.

|   | cases | lettre |
|---|---|---|
| porte-avions  |  5  | p  |
|  croiseur |  4 | c  | 
|  contre-torpilleur | 3 | o  |
|  sous-marin | 3 | s  |
|  torpilleur | 2  | t  |
|  case vide | 1  | ~  |


Votre fonction check_grid dans le fichier /src/main/java/exercice_2018_02/AppCandidat.java devra, à partir d'un tableau fourni en paramètre, retourner true ou false en fonction de la validité de la grille.

Votre implémentation sera testé avec plusieurs grilles aléatoires (valides et non-valides). Le test s'arrêtera automatiquement dès qu'une grille n'aura pas obtenu le bon résultat. Et cette grille vous sera affichée.
C'est à dire que si votre implémentation renvoit true (donc est valide) mais la grille ne l'est pas, vous aurez l'affichage de la grille que votre vérification a mal identifiée. Il faudra donc observer et en tirer les leçons pour la prochaine itération.

## Exemples
Voici quelques exemples de grilles et leur validité. Vous trouverez ces exemples dans le dossier inputs/ de l'exercice.

### Grille valide

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | t | t | p | ~ | ~ | ~ |
| o | o | o | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| c | c | c | c | ~ | ~ | ~ | ~ | ~ | ~ |

### Grilles non valides

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | s | ~ | t | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | s | ~ | t | ~ | ~ | p | ~ | ~ |
| ~ | ~ | s | ~ | ~ | ~ | ~ | p | ~ | ~ |
| o | o | o | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | c | c | c | c | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| s | s | s | s | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | o | ~ | c | c | c |
| ~ | t | t | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | s | ~ | ~ |
| ~ | ~ | ~ | ~ | c | o | c | s | c | c |
| ~ | t | t | ~ | ~ | o | ~ | s | ~ | ~ |
| ~ | p | ~ | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |


etc...

A vous de jouer et de trouver la façon la plus simple de valider/invalider une grille fournie !