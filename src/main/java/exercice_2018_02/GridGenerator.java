package exercice_2018_02;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GridGenerator {

	private static final char WATER = '~';

	public static void main(String[] args) throws IOException {
		char[][] grid = new char[10][10];

		String[] outputs = new String[] { "inputs/1.csv", "inputs/2.csv", "inputs/3.csv", "inputs/4.csv" };

		for (String outputPath : outputs) {
			// Using an array, it should be modified (NOT a copy)
			generateValidGrid(grid, outputPath);
			fromMatrixToFile(grid, outputPath);
		}
	}

	public static void generateValidGrid(char[][] grid, String outputPath) {
		// Fill grid with WATER!
		initGrid(grid);

		// We will put vessels randomly in the grid
		Random random = new Random();
		boolean result;
		do {
			result = putVessel(grid, random, 5, 'p');
		} while (!result);
		do {
			result = putVessel(grid, random, 4, 'c');
		} while (!result);
		do {
			result = putVessel(grid, random, 3, 'o');
		} while (!result);
		do {
			result = putVessel(grid, random, 3, 's');
		} while (!result);
		do {
			result = putVessel(grid, random, 2, 't');
		} while (!result);
		// All vessels have been set, grid is ready

	}

	private static void initGrid(char[][] grid) {
		for (int i = 0; i < grid.length; i++) {
			char[] cs = grid[i];
			for (int j = 0; j < cs.length; j++) {
				cs[j] = WATER;
			}
		}
	}

	private static boolean putVessel(char[][] grid, Random random, int vesselLength, char vesselId) {
		int row = random.nextInt(10);
		int col = random.nextInt(10);
		if (grid[row][col] != WATER) {
			return false;
		} else {
			if (random.nextInt(2) == 1) {
				if (tryEast(grid, vesselLength, row, col)) {
					putEast(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (trySouth(grid, vesselLength, row, col)) {
					putSouth(grid, vesselLength, row, col, vesselId);
					return true;
				}
			} else {
				if (trySouth(grid, vesselLength, row, col)) {
					putSouth(grid, vesselLength, row, col, vesselId);
					return true;
				}
				if (tryEast(grid, vesselLength, row, col)) {
					putEast(grid, vesselLength, row, col, vesselId);
					return true;
				}

			}
		}
		return false;
	}

	private static boolean tryEast(char[][] grid, int vesselLength, int row, int col) {
		try {
			if (grid[row][col + vesselLength - 1] == WATER) {
				for (int i = col + 1; i < col + vesselLength - 1; i++) {
					if (grid[row][i] != WATER) {
						return false;
					}
				}
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return false;
	}

	private static void putEast(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		for (int i = col; i < col + vesselLength; i++) {
			grid[row][i] = vesselId;
		}
	}

	private static boolean trySouth(char[][] grid, int vesselLength, int row, int col) {
		try {
			if (grid[row + vesselLength - 1][col] == WATER) {
				for (int i = row + 1; i < row + vesselLength - 1; i++) {
					if (grid[i][col] != WATER) {
						return false;
					}
				}
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return false;
	}

	private static void putSouth(char[][] grid, int vesselLength, int row, int col, char vesselId) {
		for (int i = row; i < row + vesselLength; i++) {
			grid[i][col] = vesselId;
		}
	}

	public static void printGrid(char[][] inputMatrix) {
		for (int i = 0; i < inputMatrix.length; i++) {
			char[] ds = inputMatrix[i];
			System.out.print(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				System.out.print(" | " + ds[j]);
			}
			System.out.println();
		}
	}

	/**
	 * Write the content of <code>inputMatrix</code> to a file referenced by
	 * <code>path</code>
	 * 
	 * @param inputMatrix
	 * @param path
	 * @throws IOException
	 */
	public static void fromMatrixToFile(char[][] inputMatrix, String path) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
		for (int i = 0; i < inputMatrix.length; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			char[] ds = inputMatrix[i];
			stringBuilder.append(ds[0]);
			for (int j = 1; j < ds.length; j++) {
				char d = ds[j];
				stringBuilder.append(",").append(d);
			}
			bufferedWriter.write(stringBuilder.toString());
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

}
