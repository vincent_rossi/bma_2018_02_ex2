package exercice_2018_02;


import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Runner {

	@Test
	public void test_d() {
		long start = System.currentTimeMillis();

		AppTest instance = new AppTest();
		Class appClass = instance.getClass();

		Result result = JUnitCore.runClasses(appClass);

		// prepare the failed test list
		ArrayList<String> fails = new ArrayList<String>();
		for (Failure failure : result.getFailures()) {
			String str = failure.toString();
			str = str.substring(0, str.indexOf("("));
			fails.add(str);
		}

		// get the test list
		Method[] methods = appClass.getMethods();
		for (int i = 0; i < methods.length; i++) {
			String testName = methods[i].getName();
			boolean isTest = testName.contains("test");
			if (isTest == true) {
				if (fails.indexOf(testName) != -1) {
					System.out.println("@fail(Coderpower) : " + testName);
				} else {
					System.out.println("@pass(Coderpower) : " + testName);
				}

			}
		}
		
		System.out.println("Took: " + (System.currentTimeMillis() - start) + "ms");

	}
}
