package exercice_2018_02;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class AppTest {

	@Test
	public void test_grid() throws IOException {

		File file = new File("inputs");
		File[] listFiles = file.listFiles();
		List<File> asList = Arrays.asList(listFiles);

		Collections.shuffle(asList);

		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		for (File file2 : asList) {
			if (file2.isFile()) {
				char[][] fromFileToMatrix = Utils.fromFileToMatrix(file2.getAbsolutePath());
				boolean appRefCheck = appRef.checkGrid(fromFileToMatrix);
				boolean appCandidatCheck = appToTest.checkGrid(fromFileToMatrix);

				try {
					assertEquals(appRefCheck, appCandidatCheck);
				} catch (AssertionError e) {
					System.out.println("This following grid failed test (should have returned " + appRefCheck + ")");
					Utils.printGrid(fromFileToMatrix);
					fail();
				}
			}
		}

	}
}
